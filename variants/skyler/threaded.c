#ifndef FUN_NAME
#define FUN_NAME ThreadedTranspose
#endif

#define BLOCK_SIZE 48
#define NUM_THREADS 16
#define KERNEL \
for (int i = r; i < m && i < r + BLOCK_SIZE; ++i) \
{ \
  for (int j = c; j < n && j < c + BLOCK_SIZE; ++j) \
  { \
    dst[j * rs_d + i * cs_d] = src[i * rs_s + j * cs_s]; \
  } \
}

#include <pthread.h>
#include <stdio.h>

typedef struct ThreadArgs
{
    int m;
    int n;
    float *src;
    int rs_s;
    int cs_s;
    float *dst;
    int rs_d;
    int cs_d;
} ThreadArgs;

int r, c;
pthread_mutex_t mutex;

void* ThreadOperation(void* args)
{
    ThreadArgs *a = (ThreadArgs *)args;
    int lr, lc, re, ce;

    while (1)
    {
        pthread_mutex_lock(&mutex);

        // Compute starting point of chunk
        c += BLOCK_SIZE;
        if (c >= a->n)
        {
            c = -BLOCK_SIZE;
            r += BLOCK_SIZE;
        }
        lr = r;
        lc = c;

        pthread_mutex_unlock(&mutex);

        if (lr >= a->m)
        {
            break;
        }

        // Computing lower bounds of chunk
        re = lr + BLOCK_SIZE;
        re = (a->m > re) ? re : a->m;
        ce = lc + BLOCK_SIZE;
        ce = (a->n > ce) ? ce : a->n;

        for (int i = lr; i < re; ++i)
        {
            for (int j = lc; j < ce; ++j)
            {
                a->dst[j * a->rs_d + i * a->cs_d] = 
                a->src[i * a->rs_s + j * a->cs_s];
            }
        }
    }

    pthread_exit(NULL);
}

void FUN_NAME(
  int m, 
  int n,
  float *src,
  int rs_s, int cs_s,
  float *dst,
  int rs_d, int cs_d
) {
    int i;
    pthread_t tid[NUM_THREADS];

    ThreadArgs args = {
        .m = m, .n = n,
        .src = src,
        .rs_s = rs_s, .cs_s = cs_s,
        .dst = dst,
        .rs_d = rs_d, .cs_d = cs_d
    };

    // Create each thread
    r = 0;
    c = -BLOCK_SIZE;
    pthread_mutex_init(&mutex, NULL);
    for (i = 0; i < NUM_THREADS; i++)
    {
        pthread_create(&tid[i], NULL, &ThreadOperation, (void*)&args);
    }

    // Join each thread
    for (i = 0; i < NUM_THREADS; i++)
    {
        pthread_join(tid[i], NULL);
    }
    pthread_mutex_destroy(&mutex);
}