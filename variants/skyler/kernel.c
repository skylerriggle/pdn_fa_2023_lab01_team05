#ifndef FUN_NAME
#define FUN_NAME NormalKernel
#endif

#define BLOCK_SIZE 16
#define KERNEL \
for (int i = r; i < m && i < r + BLOCK_SIZE; ++i) \
{ \
  for (int j = c; j < n && j < c + BLOCK_SIZE; ++j) \
  { \
    dst[j * rs_d + i * cs_d] = src[i * rs_s + j * cs_s]; \
  } \
}

void FUN_NAME(
  int m, 
  int n,
  float *src,
  int rs_s, int cs_s,
  float *dst,
  int rs_d, int cs_d
) {
  // Iterate through each "block"
  for (int r = 0; r < m; r += BLOCK_SIZE)
  {
    for (int c = 0; c < n; c += BLOCK_SIZE)
    {
      KERNEL;
    }
  }
}