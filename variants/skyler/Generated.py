import os
import pandas as pd
import re
from matplotlib import pyplot as plt

ROOT = os.path.dirname(__file__)
FILENAME = ROOT + "/Generated.c"
CSV = ROOT + "/../../result_bench_op2_var03.csv"

os.system("make cleanall")

if not os.path.exists(FILENAME):
    with open(FILENAME, 'x') as file:
        file.write("""#ifndef FUN_NAME
#define FUN_NAME GeneratedKernel
#endif

#define BLOCK_SIZE 16

#define KERNEL for (int i = r; i < m && i < r + BLOCK_SIZE; ++i) {     for (int j = c; j < n && j < c + BLOCK_SIZE; ++j)     {         dst[j * rs_d + i * cs_d] = src[i * rs_s + j * cs_s];     } }

void FUN_NAME(
int m, 
int n,
float *src,
int rs_s, int cs_s,
float *dst,
int rs_d, int cs_d
) {
    // Iterate through each "block"
    for (int r = 0; r < m; r += BLOCK_SIZE)
    {
        for (int c = 0; c < n; c += BLOCK_SIZE)
        {
            KERNEL;
        }
    }
}
""")
        

MIN = 16
MAX = 1024
STEP = 16
best = MIN
best_gbps = -1
results = []

def write_code(cur):
    with open(FILENAME, 'r') as file:
        data = file.readlines()

    data[4] = "#define BLOCK_SIZE " + str(cur) + "\n"
    
    with open(FILENAME, 'w') as file:
        data = file.writelines(data)

def bench():
    content = ''
    with open(CSV, 'r') as file:
        content += file.read()
    
    content = re.sub(", ", ",", content)
    content = re.sub("\nm,n,rs_src,cs_src,rs_dst,cs_dst,GB_per_s", "", content)

    with open(CSV, 'w') as file:
        file.write(content)

    df = pd.read_csv(CSV)
    
    val = 0
    for _, row in df.iterrows():
        val += row["GB_per_s"]

    return val

cur = MIN
while cur <= MAX:
    write_code(cur)
    os.system("make run_bench_op2_var03")
    val = bench()
    results.append(val)
    if val > best_gbps:
        best_gbps = val
        best = cur
    os.system("make cleanall")
    cur += STEP

write_code(best)
os.system("make run_bench_op2_var03")
print("BEST RESULT: " + str(best))

idxs = []
for idx in range(0, len(results)):
    idxs.append(MIN + (idx * STEP))

plt.xlabel("Block Size")
plt.ylabel("GB/s")
plt.plot(idxs, results)
plt.savefig('PlotImage.png')
plt.show()