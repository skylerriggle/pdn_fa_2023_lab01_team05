/*

  m,n >= 0:   dimension of src matrix
  m: number of rows
  n: number of columns

  float* src: source matrix (m-by-n matrix)
  rs_s, cs_s >= 1: row and column stride of source matrix
  rs_s: distance in memory between rows (rs_s = 1 --> column major ordering)
  cs_s: distance in memory between columns (cs_s = 1 --> row major ordering)

  float* dst: destination matrix (n-by-m matrix)
  rs_d, cs_d >= 1: row and column stride of destination matix

  NOTE: This is an out-of-place transposition meaning src and
        dst WILL NOT OVERLAP.

*/

#ifndef FUN_NAME
#define FUN_NAME baseline_transpose
#endif

//Block transpose 
void blockTranspose(int blockSize, int rs_s, int cs_s, int rs_d, int cs_d, int r, int c, float *src, float *dst){
 for (int a = r; a < r + blockSize; a++) 
    for (int b = c; b < c + blockSize; b++) {
        if (a != b) 
            dst[b*rs_d + a*cs_d] = src[a*rs_s + b*cs_s];
        else
            dst[a*rs_d + b*cs_d] = src[a*rs_s + b*cs_s];
    }
}

void FUN_NAME( int m, int n,
        float *src,
        int rs_s, int cs_s,
        float *dst,
        int rs_d, int cs_d)
{
    int blockSize = 16;
    for (int i = 0; i < m; i += blockSize) 
        for (int j = 0; j < n; j += blockSize) 
            blockTranspose(blockSize, rs_s, cs_s, rs_d, cs_d, i, j, src, dst);
}