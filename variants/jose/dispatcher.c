/*

  m,n >= 0:   dimension of src matrix
  m: number of rows
  n: number of columns

  float* src: source matrix (m-by-n matrix)
  rs_s, cs_s >= 1: row and column stride of source matrix
  rs_s: distance in memory between rows (rs_s = 1 --> column major ordering)
  cs_s: distance in memory between columns (cs_s = 1 --> row major ordering)

  float* dst: destination matrix (n-by-m matrix)
  rs_d, cs_d >= 1: row and column stride of destination matix

  NOTE: This is an out-of-place transposition meaning src and
        dst WILL NOT OVERLAP.

*/

/*

  This variant will be used for fulfilling task 1

*/

#ifndef FUN_NAME
#define FUN_NAME Dispatcher
#endif

#include <stdio.h>
#include <stdbool.h>

// Check if the matrix is column-major
bool col_maj(int rs, int cs) {
    return (cs >= rs);
}

// Check if the matrix is row-major
bool row_maj(int rs, int cs) {
    return (rs >= cs);
}

// Check if it's neither
bool gen_str(int rs, int cs) {
    return (rs != cs);
}

void FUN_NAME(int m, int n, float *src, int rs_s, int cs_s, float *dst, int rs_d, int cs_d)
{
	if (col_maj(rs_s, cs_s) && col_maj(rs_d, cs_d))
	{
		for (int j = 0; j < n; ++j)
		{
			for (int i = 0; i < m; ++i)
			{
				dst[j *rs_d + i *cs_d] = src[i *rs_s + j *cs_s];
			}
		}
	}
	else if (row_maj(rs_s, cs_s) && row_maj(rs_d, cs_d))
	{
		for (int i = 0; i < m; ++i)
		{
			for (int j = 0; j < n; ++j)
			{
				dst[j *cs_d + i *rs_d] = src[i *cs_s + j *rs_s];
			}
		}
	}
	else if (col_maj(rs_s, cs_s) && row_maj(rs_d, cs_d))
	{
		for (int i = 0; i < m; ++i)
		{
			for (int j = 0; j < n; ++j)
			{
				dst[j *cs_d + i *rs_d] = src[i *rs_s + j *cs_s];
			}
		}
	}
	else if (row_maj(rs_s, cs_s) && col_maj(rs_d, cs_d))
	{
		for (int j = 0; j < n; ++j)
		{
			for (int i = 0; i < m; ++i)
			{
				dst[j *rs_d + i *cs_d] = src[j *cs_s + i *rs_s];
			}
		}
	}
	else
	{
		for (int i = 0; i < m; ++i)
		{
			for (int j = 0; j < n; ++j)
			{
				dst[j *rs_d + i *cs_d] = src[i *rs_s + j *cs_s];
			}
		}
	}
}