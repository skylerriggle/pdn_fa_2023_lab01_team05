/*

  m,n >= 0:   dimension of src matrix
  m: number of rows
  n: number of columns

  float* src: source matrix (m-by-n matrix)
  rs_s, cs_s >= 1: row and column stride of source matrix
  rs_s: distance in memory between rows (rs_s = 1 --> column major ordering)
  cs_s: distance in memory between columns (cs_s = 1 --> row major ordering)

  float* dst: destination matrix (n-by-m matrix)
  rs_d, cs_d >= 1: row and column stride of destination matix

  NOTE: This is an out-of-place transposition meaning src and
        dst WILL NOT OVERLAP.

*/

/*

  This variant will be used for fulfilling task 4: Use of SIMD based kernel

*/

#ifndef FUN_NAME
#define FUN_NAME NormalKernel
#endif

#include <immintrin.h>

#define BLOCK_SIZE 16
#define KERNEL \
for (int i = r; i < m && i < r + BLOCK_SIZE; ++i) \
{ \
  for (int j = c; j < n && j < c + BLOCK_SIZE; ++j) \
  { \
            int idx_s = i*rs_s + j*cs_s;\
            int idx_d = j*rs_d + i*cs_d;\
            __m256 input00_07 = _mm256_loadu_ps(&src[idx_s]);\
            __m256 input08_15 = _mm256_loadu_ps(&src[idx_s + 8]); \
            __m256i index = _mm256_set_epi32(0,1,2,3,4,5,6,7);\
            input08_15 = _mm256_permutevar8x32_ps(input08_15, index);\
            __m256 output00_07 = _mm256_blend_ps(input00_07, input08_15, 0b11001100);\
            __m256 output08_15 = _mm256_blend_ps(input00_07, input08_15, 0b00110011);\
            __m256i index1 = _mm256_setr_epi32(0,4,7,3,1,5,6,2);\
            __m256i index2 = _mm256_setr_epi32(2,6,5,1,3,7,4,0);\
            output00_07 = _mm256_permutevar8x32_ps(output00_07, index1);\
            output08_15 = _mm256_permutevar8x32_ps(output08_15, index2);\
            _mm256_storeu_ps(&dst[idx_d], output00_07);\
            _mm256_storeu_ps(&dst[idx_d + 8], output08_15); \
  } \
}

void FUN_NAME(
  int m, 
  int n,
  float *src,
  int rs_s, int cs_s,
  float *dst,
  int rs_d, int cs_d
) {
  // Iterate through each "block"
  for (int r = 0; r < m; r += BLOCK_SIZE)
  {
    for (int c = 0; c < n; c += BLOCK_SIZE)
    {
      KERNEL;
    }
  }
}